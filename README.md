# Sensoric Data Quality
The use case cardiology sensoric data is collected from study participants through Apple Watch and transmitted to FHIR stores of the MeDICs of different Partner Sites involved in the project. FHIR is the data exchange model between the sensoric health-App and the MeDICs. Two continuous measurements (steps and heart rates) are collected from the subject on daily basis. Also, there are two questionnaires (daily survey and biweekly cardiology-related information). The accumulated data are bundled into FHIR Document following the MHD initiative.

## ETL
The FHIR documents are extracted in xml format and transformed into FHIR transactions using XSLT before sending back to the FHIR endpoint to decompose the resources within each document. The extraction and transformation are realised using batch script.

## Loading resources to R platform
All decomposed resources including observations and questionnaires are loaded into R. The requested bundles are batched using the `max_bundles` attribute to avoid overload. Please see [FHIRCrackR](https://github.com/POLAR-fhiR/fhircrackr) for further details.

## Data Quality Indicators
We considered three indicators to assess the quality of the sensoric data.

* _Completeness_ of recorded observations per day against the expected observations. Three measurements are expected per day including daily survey, heart rate (HR), and steps. We defined Boolean rules for all possible scenarios including 1) all three observations available, 2) only two of the observations are available, 3) only one of the observations is captured, and 4) no observations were captured for the study day.

* _Plausibility_ of HR and steps measurements with respect to duration. Outliers in Heart rates in *bpm* are investigated for correctness.

* _Conformance_ to study-specific rules including daily survey and cardiology questionnaire.
